<?php
use MysticTheme\Lib\BeaverBuilder\Helpers as BBHelpers;

$is_bb_enabled = BBHelpers::isBuilderEnabled();
$container_classes = $is_bb_enabled ? 'bb-container' : 'container content-container';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class() ?>>
  <div class="container title-container">
    <h1 style="position: absolute; left: -9999px;">
      <?php bloginfo('name') ?>
    </h1>
    <?php
      if (!$is_bb_enabled) {
    ?>
      <h2 class="post-title">
        <?php the_title(); ?>
      </h2>
    <?php
        }
    ?>
  </div>
  <div class=<?php echo $container_classes; ?>>
    <?php the_content(); ?>
  </div>
</article>