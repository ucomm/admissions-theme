<?php
use MysticTheme\Lib\BeaverBuilder\Helpers as BBHelpers;
$is_bb_enabled = BBHelpers::isBuilderEnabled();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="container title-container">
    <a href="<?php the_permalink(); ?>">
      <?php
      if (!$is_bb_enabled) {
      ?>
        <h2 class="post-title">
          <?php the_title(); ?>
        </h2>
      <?php
      }
      ?>
    </a>
  </div>
  <div class="container content-container">
    <?php the_excerpt(); ?>
  </div>
</article>