<article id="post-<?php the_ID(); ?>" <?php post_class() ?>>
  <div class="container title-container">
    <h2 class="post-title">
      <?php the_title(); ?>
    </h2>
  </div>
  <div class="container content-container">
    <?php the_excerpt(); ?>
  </div>
</article>