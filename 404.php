<?php get_header(); ?>

<main>
  <section class="row">
    <div id="error-page-form-wrapper" class="container error-container">
      <h1>Page not found</h1>
      <p>The page you requested does not exist.</p>
      <?php get_search_form(); ?>
    </div>
  </section>
</main>

<?php get_footer(); ?>