<?php

use MysticTheme\Lib\Header;

$header = new Header();

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <?php $header->createMetaTags(); ?>
  <title>
    <?php
    wp_title('');
    if (wp_title('', false)) {
      echo ' : ';
    }
    bloginfo('name');
    ?>
  </title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="page-wrapper">
    <?php
    $header->doCookieNotice();
    $header->doAlertBanner();
    ?>
    <a href="#main-content" class="screen-reader skip-nav">Skip Navigation</a>
    <header>
      <div class="row banner-row">
        <?php $header->displayBanner(); ?>
      </div>
      <section class="row header-row">
        <div class="container upper-header-container">
          <div>
            <a class="site-name" href="<?php echo home_url(); ?>">
              <span><?php bloginfo('name'); ?></span>
            </a>
          </div>
          <?php echo $header->createTitleAreaMenu(); ?>
          <div class="mobile-trigger-container">
            <button aria-expanded="false" aria-haspopup="true" aria-owns="mobile-nav-container" id="mobile-trigger">Menu</button>
          </div>
          <div id="mobile-nav-container">
            <div class="mobile-nav-menu-wrapper">
              <?php echo $header->createMobileNavMenu(); ?>
              <hr />
              <?php echo $header->createTitleAreaMenu('mobile-title-menu'); ?>
              <hr />
              <div class="container search-container">
                <?php get_search_form(); ?>
              </div>
            </div>
          </div>
      </section>
      <section class="row header-row">
        <?php echo $header->createMainNavigation(); ?>
      </section>


    </header>