const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: ['./src/js/index.js'],
  output: {
    path: process.env.NODE_ENV === 'production' ?
      path.resolve(__dirname, 'build') :
      path.resolve(__dirname, 'dev-build'),
    filename: 'index.js'
  },
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.json']
  }
}