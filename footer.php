<?php

use MysticTheme\Lib\Footer;

$footer = new Footer();

?>

<footer>
  <section class="row footer-upper-row">
    <div>
      <div class="container wordmark-container">
        <?php $footer->displayWordmark(); ?>
      </div>
    </div>
    <div>
      <div class="container widget-container">
        <div class="address-social">
          <?php
          $footer->displayWidget('footer_widget_1');
          $footer->displayWidget('footer_social_widget');
          ?>
        </div>
        <div>
          <?php $footer->displayWidget('footer_widget_2'); ?>
        </div>
        <div>
          <?php $footer->displayWidget('footer_widget_3'); ?>
        </div>
        <div>
          <?php $footer->displayWidget('footer_widget_4'); ?>
        </div>
      </div>
    </div>
  </section>
  <section class="row footer-lower-row">
    <?php echo $footer->displayLowerMenu(); ?>
  </section>
</footer>

<?php wp_footer(); ?>
</div>
</body>

</html>