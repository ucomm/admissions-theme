<?php

define('MYSTIC_THEME_DIR', get_stylesheet_directory());
define('MYSTIC_THEME_URL', get_stylesheet_directory_uri());

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once('lib/AbstractAssetLoader.php');
// require_once('lib/Admin/MenuPage.php');
// require_once('lib/Admin/Options.php');
require_once('lib/Admin/ThemeSupports.php');
require_once('lib/BeaverBuilder/FormHelpers.php');
require_once('lib/BeaverBuilder/Helpers.php');
require_once('lib/BeaverBuilder/StyleHelpers.php');
require_once('lib/ContentManager.php');
require_once('lib/Footer.php');
require_once('lib/Header.php');
require_once('lib/Menus.php');
require_once('lib/ScriptLoader.php');
require_once('lib/StyleLoader.php');
require_once('lib/Widgets/SocialWidget.php');
require_once('lib/Widgets/Widgets.php');

if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
  require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
  require_once(ABSPATH . 'vendor/autoload.php');
} else {
  require_once('vendor/autoload.php');
}

use MysticTheme\Lib\Admin\ThemeSupports;
use MysticTheme\Lib\BeaverBuilder\Helpers as BBHelpers;
use MysticTheme\Lib\Menus;
use MysticTheme\Lib\ScriptLoader;
use MysticTheme\Lib\StyleLoader;
// use MysticTheme\Lib\Admin\MenuPage;
use MysticTheme\Lib\Widgets\Widgets;

$scriptLoader = new ScriptLoader();
$scriptLoader->enqueueAssets();

$styleLoader = new StyleLoader();
$styleLoader->enqueueAssets();

if (is_admin()) {
  // add settings page if necessary
  // $menuPage = new MenuPage();
  // $menuPage->createMenuPage();

  $scriptLoader->enqueueAdminAssets();
  $styleLoader->enqueueAdminAssets();
}

$menus = new Menus();
$menus->handleMenusAfterSetup();

$widgets = new Widgets();
$widgets->widgetsInit();

$themeSupports = new ThemeSupports();
$themeSupports->afterSetupTheme();
$themeSupports->initPostTypeSupport();

if (class_exists('FLBuilder')) {
  $bb_helpers = new BBHelpers();
  $bb_helpers->addCustomFonts();
  $bb_helpers->filterSettingsForms();
  $bb_helpers->filterModuleCSS();
}