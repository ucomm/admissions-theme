<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php is_front_page() ? bloginfo('description') : the_excerpt(); ?>">
<?php
if (!class_exists('Smartcrawl_Loader') || !is_plugin_active('wpmu-dev-seo/wpmu-dev-seo.php')) {
?>
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php the_permalink(); ?>">
  <meta property="og:title" content="University of Connecticut <?php is_front_page() ? bloginfo('name') : the_title(); ?>">
  <meta property="og:description" content="<?php is_front_page() ? bloginfo('description') : the_excerpt(); ?>">
  <meta property="og:image" content="<?php echo (has_post_thumbnail() ? the_post_thumbnail_url() : MYSTIC_THEME_URL . '/images/oak-leaf-blue.jpg'); ?>">
  <meta property="og:image:alt" content="<?php echo (has_post_thumbnail() ? get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) : ''); ?>">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="<?php the_permalink(); ?>">
  <meta name="twitter:title" content="University of Connecticut <?php is_front_page() ? bloginfo('name') : the_title(); ?>">
  <meta name="twitter:description" content="<?php is_front_page() ? bloginfo('description') : the_excerpt(); ?>">
  <meta name="twitter:image" content=<?php echo (has_post_thumbnail() ? the_post_thumbnail_url() : MYSTIC_THEME_URL . '/images/oak-leaf-blue.jpg'); ?>">
  <meta name="twitter:image:alt" content="<?php echo (has_post_thumbnail() ? get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) : ''); ?>">
<?php
}
?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />