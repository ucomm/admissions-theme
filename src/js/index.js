// enable the javascript for the main nav area
const navMenuIDs = [
  'am-main-menu',
  'am-mobile-nav'
]

navMenuIDs.forEach(menuId => {
  const navigation = new Navigation({ menuId, click: true })
  navigation.init()
})

// handle mobile nav trigger events
const mobileTrigger = document.querySelector('#mobile-trigger')
const mobileNavContainer = document.querySelector('#mobile-nav-container')

document.addEventListener('keydown', evt => {

  const mobileNavOpen = mobileNavContainer.classList.contains('open')
  if (evt.key !== 'Escape' || !mobileNavOpen) return

  if (mobileNavOpen) {
    mobileTrigger.innerText = 'MENU'
    mobileTrigger.setAttribute('aria-expanded', 'false')
    mobileNavContainer.classList.remove('open')
  }
})

mobileTrigger.addEventListener('click', () => {

  mobileTrigger.innerText = mobileTrigger.innerText === 'MENU' ?
    'CLOSE' : 'MENU'
  
  mobileTrigger.getAttribute('aria-expanded') === 'true' ?
    mobileTrigger.setAttribute('aria-expanded', 'false') :
    mobileTrigger.setAttribute('aria-expanded', 'true')
  
  mobileNavContainer.classList.toggle('open')
})

