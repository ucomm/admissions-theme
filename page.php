<?php

use MysticTheme\Lib\BeaverBuilder\Helpers as BBHelpers;

$is_bb_enabled = BBHelpers::isBuilderEnabled();

$layout = $is_bb_enabled ? 'default.php' : 'sidebar.php';

include(MYSTIC_THEME_DIR . '/layouts/' . $layout);
