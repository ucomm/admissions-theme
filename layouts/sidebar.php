<?php

use MysticTheme\Lib\ContentManager;

$content_manager = new ContentManager();
$content_manager->getHeader();

?>

<main>
  <div class="container sidebar-container">
    <section class="row sidebar-row">
      <aside class="sidebar">
        <?php echo $content_manager->createSidebarMenu(); ?>
      </aside>
    </section>
    <section class="row sidebar-content-row">
      <?php
        $content_manager->loop('template-parts/content', 'loop');
      ?>
    </section>
  </div>
</main>

<?php $content_manager->getFooter(); ?>