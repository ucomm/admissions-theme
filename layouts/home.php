<?php

use MysticTheme\Lib\ContentManager;

$content_manager = new ContentManager();
$content_manager->getHeader();
$content_manager->excerptMoreLink();

?>

<main>
  <section class="row">
    <?php
      $content_manager->loop('template-parts/content', 'home');
    ?>
  </section>
  <section class="row">
    <?php $content_manager->displayPaginationLinks(); ?>
  </section>
</main>

<?php $content_manager->getFooter(); ?>