<?php

use MysticTheme\Lib\ContentManager;

$content_manager = new ContentManager();

$content_manager->getHeader();
$content_manager->excerptMoreLink();
?>

<main>
  <section class="row">
    <div class="container">
      <h1>Search results for: <span class="search-query"><?php the_search_query(); ?></span></h1>
    </div>
    <div id="search-page-form-wrapper" class="container search-container">
      <?php get_search_form(); ?>
    </div>
  </section>
  <section class="row">
    <?php
      $content_manager->loop('template-parts/content', 'search');
    ?>
  </section>
  <section class="row">
    <?php $content_manager->displayPaginationLinks(); ?>
  </section>
</main>

<?php $content_manager->getFooter(); ?>