<?php

namespace MysticTheme\Lib\Admin;

use WP_Query;
use MysticTheme\Lib\BeaverBuilder\Helpers;

class MenuPage {
  public function createMenuPage() {
    add_action('admin_menu', [ $this, 'prepareMenuPage' ]);
  }

  public function prepareMenuPage() {
    add_menu_page(
      __('Mystic Theme Settings', 'mystic'),
      __('Mystic Theme Settings', 'mystic'),
      'manage_options',
      'mystic-theme-settings',
      [ $this, 'getSettingsPage' ]
    );
  }

  public function getSettingsPage() {
    include MYSTIC_THEME_DIR . '/includes/admin/settings-page.php';
  }

}