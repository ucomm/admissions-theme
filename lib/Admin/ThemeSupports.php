<?php

namespace MysticTheme\Lib\Admin;

class ThemeSupports {

  public function initPostTypeSupport() {
    add_action('init', [$this, 'addPostTypeSupport']);
  }

  public function afterSetupTheme() {
    add_action('after_setup_theme', [ $this, 'addThemeSupport' ]);
  }

  public function addPostTypeSupport() {
    add_post_type_support('page', [ 'excerpt', 'revisions' ]);
  }

  public function addThemeSupport() {
    add_theme_support('post-thumbnails');
  }
}