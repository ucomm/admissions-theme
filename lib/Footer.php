<?php

namespace MysticTheme\Lib;

use MysticTheme\Lib\Menus;
use MysticTheme\Lib\Widgets\Widgets;

class Footer {
  public $menus;
  public $widgets;

  public function __construct() {
    $this->menus = new Menus();
    $this->widgets = new Widgets();
  }

  public function displayLowerMenu() {
    return $this->menus->createLowerFooterMenu();
  }

  public function displayWidget(string $id): string {
    return $this->widgets->displayWidget($id);
  }

  public function displayWordmark() {
    ob_start();
    include(MYSTIC_THEME_DIR . '/includes/wordmark.php');
    echo ob_get_clean();
  }
}