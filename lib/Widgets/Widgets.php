<?php

namespace MysticTheme\Lib\Widgets;

use MysticTheme\Lib\Widgets\SocialWidget;

class Widgets {

  private $widgets;
  private $socialWidget;

  public function __construct() {
    $this->socialWidget = new SocialWidget();
    $this->widgets = [
      [
        'name' => __('Footer Widget 1', 'mystic'),
        'id' => 'footer_widget_1',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ],
      [
        'name' => __('Footer Social Widget', 'mystic'),
        'id' => 'footer_social_widget',
      ],
      [
        'name' => __('Footer Widget 2', 'mystic'),
        'id' => 'footer_widget_2',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ],
      [
        'name' => __('Footer Widget 3', 'mystic'),
        'id' => 'footer_widget_3',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ],
      [
        'name' => __('Footer Widget 4 - block links', 'mystic'),
        'id' => 'footer_widget_4',
        'before_widget' => '<div class="widget footer-widget">',
        'after_widget' => '</div>'
      ],
    ];
  }

  public function widgetsInit() {
    add_action('widgets_init', [$this, 'createWidgets']);
    $this->socialWidget->widgetsInit();
  }

  public function displayWidget(string $id): string {
    return is_active_sidebar($id) ?
    dynamic_sidebar($id) :
    '';
  }

  public function createWidgets(): array {
    $widget_ids = $this->prepareWidgets();
    return $widget_ids;
  }

  private function prepareWidgets(): array {
    $widgets = $this->widgets;

    return array_map(function($widget) {
      return register_sidebar($widget);
    }, $widgets);
  }
}