<?php

namespace MysticTheme\Lib\Widgets;

use WP_Widget;

class SocialWidget extends WP_Widget {

  public $args;

  public function __construct() {

    $this->args = [];

    parent::__construct(
      $this->getID(),
      $this->getName(),
      [
        'description' => __('Creates social icons with links for UConn themes.', 'mystic')
      ],
      [
        'id_base' => 'uconn-social-widget'
      ]
    );
  }

  /**
   * Display a social link with an icon.
   *
   * @param array $args - arguments to affect how the widget is displayed
   * @param array $instance - the saved data from the widget
   * @return void
   */
  public function widget($args, $instance) {
    ob_start();
    include(MYSTIC_THEME_DIR . '/includes/footer/footer-social-links.php');
    echo ob_get_clean();
    return;
  }

  /**
   * Create the form for the widget
   *
   * @param array $instance - the currently stored data
   * @return void
   */
  public function form($instance) {
    ob_start();
    include(MYSTIC_THEME_DIR . '/includes/admin/social-widget-form.php');
    echo ob_get_clean(); 
  }

  /**
   * Prepare to update the widget
   *
   * @param array $new_instance - the data to save
   * @param array $old_instance - previous data
   * @return array
   */
  public function update($new_instance, $old_instance) {
    $instance['social-icon'] = (!empty($new_instance['social-icon'])) ? $new_instance['social-icon'] : '';
    $instance['social-link'] = (!empty($new_instance['social-link'])) ? esc_url_raw($new_instance['social-link']) : '';
    $instance['a11y-title'] = (!empty($new_instance['a11y-title'])) ? sanitize_text_field($new_instance['a11y-title']) : '';
    return $new_instance;
  }

  public function widgetsInit() {
    add_action('widgets_init', [$this, 'registerWidget']);
  }

  public function registerWidget() {
    register_widget($this);
  }

  public function getSettings() {
    return $this->get_settings();
  }

  private function getID() {;
    return 'uconn-social-widget';
  }

  private function getName() {
    return 'UConn Social Widget';
  }

  /**
   * Get a social icon svg from a widget setting
   *
   * @param string $icon
   * @return string
   */
  private function getSVG(string $icon): string {
    $path = MYSTIC_THEME_DIR . '/images/icons/';
    switch ($icon) {
      case 'email-subscribe':
      case 'facebook':
      case 'flickr':
      case 'instagram':
      case 'linkedin':
      case 'snapchat':
      case 'twitter':
      case 'youtube':
        $path .= $icon . '.svg';
        break;
      default:
        $path .= 'fallback-link.svg';
        break;
    }
    return file_get_contents($path);
  }
}