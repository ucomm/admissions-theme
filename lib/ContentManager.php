<?php

namespace MysticTheme\Lib;

use MysticTheme\Lib\Menus;

class ContentManager {
  public function loop(string $slug = 'template-parts/content', string $name = 'loop', array $args = []) {
    if (have_posts()) {
      while (have_posts()) {
        the_post();
        get_template_part($slug, $name, $args);
      }
    } else {
      get_template_part($slug, 'none');
    }
  }

  public function excerptMoreLink() {
    add_filter('excerpt_more', [$this, 'prepareMoreLink']);
  }

  public function prepareMoreLink(string $more): string {

    global $post;

    $more = ' <a class="moretag" href="' . get_permalink($post->ID) . '" aria-label="Read more about ' . get_the_title($post->ID) . '">Learn more...</a>';

    return $more;
  }

  public function displayPaginationLinks() {
    ob_start();
    include(MYSTIC_THEME_DIR . '/includes/post-pagination.php');
    echo ob_get_clean();
  }

  public function getFooter() {
    get_footer();
  }

  public function getHeader() {
    get_header();
  }

  public function createSidebarMenu() {
    return (new Menus)->createSidebarMenu();
  }
}