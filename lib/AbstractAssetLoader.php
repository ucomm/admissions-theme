<?php 

namespace MysticTheme\Lib;

abstract class AbstractAssetLoader {
  protected $handle;
  protected $adminHandle;
  protected $scriptsPath;
  protected $stylesPath;

  public function __construct() {
    $this->handle = $this->setHandle();
    $this->adminHandle = $this->setAdminHandle();

    if ($_SERVER['HTTP_HOST'] !== 'localhost') {
      $this->scriptsPath = MYSTIC_THEME_URL . '/dev-build/js/index.js';
      $this->stylesPath = MYSTIC_THEME_URL . '/dev-build/css/main.css';
    } else {
      $this->scriptsPath = MYSTIC_THEME_URL . '/build/js/index.js';
      $this->stylesPath = MYSTIC_THEME_URL . '/build/css/main.css';
    }
  }

  /**
   * Enqueue the assets defined by the enqueue method.
   *
   * @return void
   */
  public function enqueueAssets() {
    add_action('wp_enqueue_scripts', [$this, 'enqueue']);
  }

  public function enqueueAdminAssets() {
    add_action('admin_enqueue_scripts', [$this, 'adminEnqueue']);
  }

  /**
   * Register and enqueue styles or scripts
   *
   * @return void
   */
  abstract function enqueue();

  abstract function adminEnqueue(string $hook);

  protected function setHandle() {
    return 'mystic';
  }

  protected function setAdminHandle() {
    return 'mystic-admin';
  }
}