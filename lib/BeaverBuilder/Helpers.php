<?php

namespace MysticTheme\Lib\BeaverBuilder;

use FLBuilderModel;
use MysticTheme\Lib\BeaverBuilder\FormHelpers;
use MysticTheme\Lib\BeaverBuilder\StyleHelpers;

class Helpers {

  private $formHelpers;
  private $styleHelpers;

  public function __construct() {
    $this->formHelpers = new FormHelpers();
    $this->styleHelpers = new StyleHelpers();
  }

  /**
   * A wrapper for checking if the beaver builder page builder is active.
   *
   * @return boolean
   */
  public static function isBuilderActive(): bool {
    return class_exists('FLBuilderModel') ? 
      FLBuilderModel::is_builder_active() : 
      false;
  }
  /**
   * A wrapper for checking if beaver builder is currently being used on a page.
   *
   * @return boolean
   */
  public static function isBuilderEnabled($post_id = null): bool {
    return class_exists('FLBuilderModel') ?
      FLBuilderModel::is_builder_enabled($post_id) :
      false;
  }

  /**
   * A wrapper to add fonts to Beaver Builder forms
   *
   * @return void
   */
  public function addCustomFonts() {
    $this->formHelpers->addCustomFonts();
  }

  /**
   * A wrapper to add fields to Beaver Builder forms
   *
   * @return void
   */
  public function filterSettingsForms() {
    $this->formHelpers->filterSettingsForms();
  }

  /**
   * A wrapper to manage filtering Beaver Builder CSS
   * 
   * @return void
   */
  public function filterModuleCSS() {
    $this->styleHelpers->filterModuleCSS();
  }
}