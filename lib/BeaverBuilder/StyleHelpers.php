<?php

namespace MysticTheme\Lib\BeaverBuilder;

class StyleHelpers {
  /**
   * A function to collect changes to module css.
   * Multiple filters can be defined here with separate callbacks.
   *
   * @return void
   */
  public function filterModuleCSS() {
    add_filter('fl_builder_render_css', [$this, 'prepareContentSliderCSS'], 10, 3);
    add_filter('fl_builder_render_css', [$this, 'prepareAccordionCSS'], 10, 3);
  }

  /**
   * Filter the CSS for content sliders.
   *
   * @param string $css - Beaver Builder's CSS settings
   * @param array $nodes - A list of all Beaver Builder objects
   * @param object $global_settings
   * @return string $css
   */
  public function prepareContentSliderCSS(string $css, array $nodes, object $global_settings): string
  {

    foreach ($nodes['modules'] as $id => $module) {
      $module_class = get_class($module);

      if ($module_class === 'FLContentSliderModule') {

        $css .= $this->addSliderGradient($module->settings->slides, $id);

      }
    }
    return $css;
  }

  /**
   * Create gradients for Beaver Builder slides
   * Padding is added to text areas because Beaver Builder only adds it if a background color is defined.
   *
   * @param array $settings - Settings from the Beaver Builder form
   * @param string $id - The Beaver Builder node ID
   * @return string $css - The CSS to add to the Beaver Builder stylesheet
   */
  private function addSliderGradient(array $settings, string $id): string {
    $css = '';
    foreach ($settings as $index => $setting) {

      if ($setting->text_bg_gradient !== '') {
        $gradient = sanitize_text_field($setting->text_bg_gradient);
        $css .= ".fl-builder-content .fl-node-" . $id . " .fl-slide-" . $index . " .fl-slide-content {
          background-image: " . $gradient . ";
          padding-top: " . $setting->text_padding_top . "px;
          padding-right: " . $setting->text_padding_right . "px;
          padding-bottom: " . $setting->text_padding_bottom . "px;
          padding-left: " . $setting->text_padding_left . "px; 
        }";
      }

    }
    return $css;
  }

  public function prepareAccordionCSS(string $css, array $nodes, object $global_settings): string {

    foreach ($nodes['modules'] as $id => $module) {
      $module_class = get_class($module);
      if ($module_class === 'FLAccordionModule') {
        $this->removeIconSettings($module->settings);
      }
    }

    return $css;
  }

  private function removeIconSettings(object $settings): object {
    unset($settings->label_icon_position);
    unset($settings->label_icon);
    unset($settings->label_active_icon);
    return $settings;
  }
}