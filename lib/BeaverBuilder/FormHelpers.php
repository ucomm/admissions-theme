<?php

namespace MysticTheme\Lib\BeaverBuilder;

use FLBuilderFontFamilies;

class FormHelpers {

  /**
   * Filter settings forms as needed according to their callback functions.
   *
   * @return void
   */
  public function filterSettingsForms() {
    add_filter('fl_builder_register_settings_form', [$this, 'prepareGradientSettings'], 10, 2);
  }

  /**
   * Create a text option for adding gradient CSS to a module.
   * Beaver Builder is ridiculous about how it manages gradients.
   *
   * @param array $form - A Beaver Builder settings form
   * @param string $id - The type of form
   * @return array $form - Either the original form or a modified form
   */
  public function prepareGradientSettings(array $form, string $id): array
  {

    if ($id === 'content_slider_slide') {

      $form['tabs']['style']['sections']['text_style']['fields']['text_bg_gradient'] = [
        'type' => 'text',
        'label' => __('Text Background Gradients'),
        'description' => __('Use this field to add a css gradient to the text background', 'mystic')
      ];
    }

    return $form;
  }

  /**
   * Add the fonts upon the init action.
   *
   * @return void
   */
  public function addCustomFonts() {
    add_action('init', [$this, 'prepareCustomFonts']);
  }

  /**
   * Prepare an array of custom fonts for the Beaver Builder settings forms.
   *
   * @return void
   */
  public function prepareCustomFonts() {
    $custom_fonts = [
      'Proxima Nova' => [
        'fallback' => 'Verdana, Arial, Helvetica, sans-serif',
        'weights' => [
          '300',
          '400',
          '600',
          '700'
        ]
      ],
      'Archivo' => [
        'fallback' => '"Proxima Nova", Verdana, Arial, Helvetica, sans-serif',
        'weights' => [
          '400',
          '600',
          '700'
        ]
      ],
      'DM Serif Display' => [
        'fallback' => 'times, serif',
        'weights' => [
          '400',
        ]
      ],
      'league-gothic' => [
        'fallback' => '"Proxima Nova", Verdana, Arial, Helvetica, sans-serif',
        'weights' => [
          '400'
        ]
      ]
    ];

    foreach ($custom_fonts as $name => $settings) {
      // Add to page builder
      if (class_exists('FLBuilderFontFamilies') && isset(FlBuilderFontFamilies::$system)) {
        FLBuilderFontFamilies::$system[$name] = $settings;
      }
    }
  }
}