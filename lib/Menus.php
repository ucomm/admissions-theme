<?php

namespace MysticTheme\Lib;

use A11y\Menu_Walker;

class Menus {

  public function handleMenusAfterSetup() {
    add_action('after_setup_theme', [$this, 'registerMenus']);
  }

  public function registerMenus() {
    $theme_menus = [
      'footer_block_links' => __('Footer Block Links', 'mystic'),
      'footer_quick_nav' => __('Footer Quick Nav Links', 'mystic'),
      'info_for' => __('Info For Menu', 'mystic'),
      'lower_footer' => __('Lower Footer', 'mystic'),
      'main_nav' => __('Main Navigation Menu', 'mystic'),
      'title_area_menu' => __('Title Area Menu', 'mystic'),
      'sidebar_menu' => __('Sidebar Menu', 'mystic')
    ];

    foreach ($theme_menus as $slug => $menu) {
      register_nav_menu($slug, $menu);
    }
  }

  public function createLowerFooterMenu() {
    return wp_nav_menu([
      'container' => 'nav',
      'container_class' => 'container menu-container lower-menu-container',
      'container_aria_label' => 'Lower Footer Menu',
      'theme_location' => 'lower_footer',
      'menu_id' => 'lower-footer-menu',
      'menu_class' => 'menu footer-menu lower-footer-menu',
      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
      'echo' => false
    ]);
  }

  public function createMainNavigationMenu() {
    return wp_nav_menu([
      'container' => 'nav',
      'container_id' => 'main-nav-menu',
      'container_class' => 'container menu-container main-menu-container',
      'container_aria_label' => 'Main Navigation Menu',
      'theme_location' => 'main_nav',
      'menu_id' => 'am-main-menu',
      'menu_class' => 'menu main-nav-menu',
      'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
      'walker' => new Menu_Walker(),
      'echo' => false
    ]);
  }

  public function createMobileNavMenu() {
    return wp_nav_menu([
      'container' => 'nav',
      'container_id' => 'mobile-nav-menu',
      'container_class' => 'menu-container mobile-menu-container',
      'container_aria_label' => 'Mobile Navigation Menu',
      'theme_location' => 'main_nav',
      'menu_id' => 'am-mobile-nav',
      'menu_class' => 'menu mobile-nav-menu',
      'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
      'walker' => new Menu_Walker(),
      'echo' => false
    ]);
  }

  public function createTitleAreaMenu(string $id = 'title-area-menu') {
    return wp_nav_menu([
      'theme_location' => 'title_area_menu',
      'menu_id' => $id,
      'menu_class' => 'menu title-area-menu',
      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
      'echo' => false      
    ]);
  }

  public function createSidebarMenu() {
    return wp_nav_menu([
      'container' => 'nav',
      'container_id' => 'sidebar-nav',
      'container_class' => 'menu-container sidebar-menu-container',
      'container_aria_label' => 'Sidebar Navigation Menu',
      'theme_location' => 'sidebar_menu',
      'menu_id' => 'sidebar-menu',
      'menu_class' => 'menu sidebar-menu',
      'items_wrap' => '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>',
      'echo' => false
    ]);
  }
}